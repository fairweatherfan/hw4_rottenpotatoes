require 'spec_helper'

describe MoviesController do
    before :each do
      Movie.delete_all
      @movie1 = FactoryGirl.create(:movie, :id => 1, :title => "Rocky", :director => nil)
      @movie2 = FactoryGirl.create(:movie, :id => 2, :title => "Splash", :director => "Ron Howard")
      @movie3 = FactoryGirl.create(:movie, :id => 3, :title => "Little Big Man", :director => nil)
      @movie4 = FactoryGirl.create(:movie, :id => 4, :title => "Cinderella Man", :director => "Ron Howard")
      @movie5 = FactoryGirl.create(:movie, :id => 5, :title => "The Davinci Code", :director => "Ron Howard")
      @movie6 = FactoryGirl.create(:movie, :id => 6, :title => "A Clockwork Orange", :director => "Stanley Kubrik")
      @movie7 = FactoryGirl.create(:movie, :id => 7, :title => "The Birds", :director => "Alfred Hitchcock")
      @movie8 = FactoryGirl.create(:movie, :id => 8, :title => "2001 A Space Oddesey", :director => "Stanley Kubrik")
      @fake_similar_movies = [mock('movie1'), mock('movie2')]
    end

  describe 'Find Movies with Same Director when movie has director data' do
    it 'should call the model method that performs the search for movies with the same director' do
      Movie.should_receive(:find_all_by_same_director).with(2).and_return(@fake_similar_movies) 
      post :similar_movies, {:id => '2'}
    end  
    it 'should select the Similar Movies template for rendering' do
      Movie.stub(:find_all_by_same_director).and_return(@fake_similar_movies)
      post :similar_movies, {:id => '2'}
      response.should render_template('similar_movies')
    end
    it 'should make the search for movies with the same director results available to that template' do
      Movie.stub(:find_all_by_same_director).and_return(@fake_similar_movies)
      post :similar_movies, {:id => '2'}
      assigns(:movies).should == @fake_similar_movies
    end
  end
  describe 'Find Movies with Same Director when movie NO director data' do
    it 'should call the model method that performs the search for movies with the same director' do
      @fake_similar_movies =[]
      Movie.should_receive(:find_all_by_same_director).with(1).and_return(@fake_similar_movies) 
      post :similar_movies, {:id => '1'}
    end  
  end
end
