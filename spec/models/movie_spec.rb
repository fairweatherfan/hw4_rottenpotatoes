require 'spec_helper'

describe Movie do
  describe '.find_all_by_same_director(id)' do
    before :each do
      Movie.delete_all
      @movie1 = FactoryGirl.create(:movie, :id => 1, :title => "Rocky", :director => nil)
      @movie2 = FactoryGirl.create(:movie, :id => 2, :title => "Splash", :director => nil)
      @movie3 = FactoryGirl.create(:movie, :id => 3, :title => "Little Big Man", :director => nil)
      @movie4 = FactoryGirl.create(:movie, :id => 4, :title => "Cinderella Man", :director => "Ron Howard")
      @movie5 = FactoryGirl.create(:movie, :id => 5, :title => "The Davinci Code", :director => "Ron Howard")
      @movie6 = FactoryGirl.create(:movie, :id => 6, :title => "A Clockwork Orange", :director => "Stanley Kubrik")
      @movie7 = FactoryGirl.create(:movie, :id => 7, :title => "The Birds", :director => "Alfred Hitchcock")
      @movie8 = FactoryGirl.create(:movie, :id => 8, :title => "2001 A Space Oddesey", :director => "Stanley Kubrik")
    end
    context 'when referenced movie has director in database' do
      it 'should return all movies by the same director' do
        Movie.find_all_by_same_director(5).should == [@movie4, @movie5]
        Movie.find_all_by_same_director(6).should == [@movie6, @movie8] 
      end
    end
    context 'when referenced movie does not have director in database'do
      it 'should return nil' do
        Movie.find_all_by_same_director(3).should == nil 
      end
    end
  end
end  
