Given /the following movies exist/ do |movies_table|
  movies_table.hashes.each do |movie|
    Movie.create! movie
  end
end

Then /the director of "(.+)" should be "(.+)"/ do |name, dir|
  Movie.find_by_title(name).director.should == dir
end

